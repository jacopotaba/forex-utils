import React from "react";

const Footer = (props: any) => {
  const footerStyle = {
    backgroundColor: "#000",
    height: "50px",
    width: "100%",
    display: "flex",
    alignItem: "center",
    justifyContent: "center",
    color: "#fff",
  };

  return (
    <div className="footer" style={footerStyle}>
      <p>{props.name}</p>
    </div>
  );
};

export default Footer;
