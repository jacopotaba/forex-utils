import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Homepage from '../../containers/Homepage/Homepage';
import Movies from '../../containers/Movies/Movies';
import Series from '../../containers/Series/Series';

const MainNavigation = (props: any) => {
	return (
		<>
			<div>
					<Switch>
						<Route exact path="/" component={Homepage} />
						<Route exact path="/movies" component={Movies} />
						<Route exact path="/series" component={Series} />
					</Switch>
			</div>
		</>
	)
}

export default MainNavigation