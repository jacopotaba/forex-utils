import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";

export interface ContainerPropsInterface {
  [x: string]: any;
}
const SimpleContainer = (props: ContainerPropsInterface) => {
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg" style={{ position: "relative", marginTop: 32, marginBottom: 32 }}>
        <Typography
          component="div"
          style={{ height: "100%" }}
        />
        <div
          className="MuiContainer-root MuiContainer-maxWidthLg"
          style={{ position: "absolute", left: 0, top: 0, zIndex: 99 }}
        >
          {props.children}
        </div>
      </Container>
    </React.Fragment>
  );
};

export default SimpleContainer;
