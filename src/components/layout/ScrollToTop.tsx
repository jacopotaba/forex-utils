import { Box, IconButton } from "@material-ui/core";
import React, { Component } from "react";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
export default class ScrollToTop extends Component<any, { is_visible: boolean }> {
  scrollComponent: any;

  constructor(props: any) {
    super(props);
    this.state = {
      is_visible: true,
    };
  }
  componentDidMount() {}


  scrollToTop() {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  render() {
    return (
      <>
        <Box
          display={{ xs: "none", md: "block" }}
          className="scroll-to-top"
          style={{ position: "fixed", bottom: 100, right: 100, zIndex: 99 }}
        >
          {this.state.is_visible && (
            <div onClick={() => this.scrollToTop()}>
              <IconButton
                style={{ background: "#3F51B5" }}
                size="medium"
                aria-label="up"
              >
                <ArrowUpwardIcon style={{ color: "#fff" }} />
              </IconButton>
            </div>
          )}
        </Box>
        <Box
          display={{ xs: "block", md: "none" }}
          className="scroll-to-top"
          style={{ position: "fixed", bottom: 10, left: 10, zIndex: 99 }}
        >
          {this.state.is_visible && (
            <div onClick={() => this.scrollToTop()}>
              <IconButton
                style={{ background: "#3F51B5" }}
                size="medium"
                aria-label="up"
              >
                <ArrowUpwardIcon style={{ color: "#fff" }} />
              </IconButton>
            </div>
          )}
        </Box>
      </>
    );
  }
}
