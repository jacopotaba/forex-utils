import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Button, Grid, Paper, Typography } from "@material-ui/core";
import API from "../../../api/ApiService";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 420,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.primary,
    },
  })
);

const CalcolaPip = (props: any) => {
  const classes = useStyles();
  let [pip, setPip]: any[] = React.useState(0);

  let firstValue: string | any = "";
  let secondValue: string | any = "";

  let volumeRefField: any;

  let query = ``;

  let valorePip: any = null

  const calculate: any = () => {
    let taxExange;

    API.forexDataFixerGet("latest", query).then((res: any) => {
      if (res && res.rates && res.rates[secondValue]) {
        taxExange = res.rates[secondValue];
        // console.log(taxExange)
        if (volumeRefField && volumeRefField.value && taxExange) {
          let moltiplicator = secondValue === "JPY" ? 0.01 : 0.0001;
          console.log(
            "valore PIP",
            volumeRefField.value * (1 / taxExange) * moltiplicator
          );
          valorePip = volumeRefField.value * (1 / taxExange) * moltiplicator;
          return setPip((pip = valorePip));
        }
      } else {
        console.log("TASSO DI CAMBIO NON TROVATO");
        return 0;
      }
    });
  };

  return (
    <div>
      {props.items ? (
        <div className={classes.root}>
          <Grid container spacing={3}>
            <Grid item xs={8}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Autocomplete
                      onChange={(event, value) => (firstValue = value)}
                      id="free-solo-demo"
                      freeSolo
                      options={Object.keys(props.items).map((option) => option)}
                      renderInput={(params: any) => (
                        <TextField
                          {...params}
                          label="Prima valuta"
                          margin="normal"
                          variant="outlined"
                        />
                      )}
                    />
                  </Paper>
                </Grid>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Autocomplete
                      onChange={(event, value: any) => {
                        secondValue = value;
                        query = `&symbols=${secondValue}`;
                      }}
                      id="free-solo-demo"
                      freeSolo
                      options={Object.keys(props.items).map((option) => option)}
                      renderInput={(params: any) => (
                        <TextField
                          {...params}
                          label="Seconda valuta"
                          margin="normal"
                          variant="outlined"
                        />
                      )}
                    />
                  </Paper>
                </Grid>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <form>
                      <TextField
                        fullWidth={true}
                        id="outlined-basic"
                        label="Dimesione scambio"
                        variant="outlined"
                        defaultValue={1000}
                        type="number"
                        inputRef={(ref) => {
                          volumeRefField = ref;
                        }}
                      />
                    </form>
                  </Paper>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={4}>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro
                voluptates provident expedita in tenetur quibusdam pariatur
                accusamus iusto numquam officiis. Eligendi fugit at earum
                accusamus animi deserunt. Asperiores, dolorum illum!
              </p>
              <Typography variant="h3" component="h2" gutterBottom>
                {pip}
              </Typography>
            </Grid>
            <Grid>
              <Button onClick={calculate} variant="contained" color="primary">
                Calcola
              </Button>
            </Grid>
          </Grid>
        </div>
      ) : null}
    </div>
  );
};

export default CalcolaPip;
