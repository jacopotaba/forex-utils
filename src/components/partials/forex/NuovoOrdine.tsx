import React from "react";
import Typography from "@material-ui/core/Typography";
import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Tab,
  Tabs,
  TextField,
  Tooltip,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import API from "../../../api/ApiService";
import LocalAtmIcon from "@material-ui/icons/LocalAtm";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import PanToolIcon from "@material-ui/icons/PanTool";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import ControlPointIcon from "@material-ui/icons/ControlPoint";
import RemoveIcon from "@material-ui/icons/Remove";
import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
import CircularProgress from "@material-ui/core/CircularProgress";

export interface NuovoOrdinePropsInterface {
  items: any;
  sendValues: any;
}

export interface NuovoOrdineStateInterface {
  firstValue: string | any;
  secondValue: string | any;
  volumeValue: null | number;
  enterPrice: null | number;
  stopLoss: null | number | any;
  takeProfit: null | number | any;
  proceedToStep2: boolean;
  tabValue: number;
  orderType: string;
  prezzoOttenuto: boolean;
  rrr: any;
  margin: any;
  moltiplicator: number;
  loading: boolean;
  scrollPos: any;
}
export default class NuovoOrdine extends React.Component<
  NuovoOrdinePropsInterface,
  NuovoOrdineStateInterface
> {
  query = ``;
  valorePip: any = null;
  enterPriceRef: any;
  stopLoss: any;
  takeProfit: any;

  leva = 400;

  constructor(props: any) {
    super(props);
    this.state = {
      orderType: "sell",
      firstValue: "",
      secondValue: "",
      volumeValue: 1000,
      enterPrice: null,
      stopLoss: null,
      takeProfit: null,
      proceedToStep2: false,
      tabValue: 0,
      prezzoOttenuto: false,
      rrr: {
        pipsInWin: 1,
        pipsInLose: 1,
        euroInWin: 1,
        euroInLose: 1,
      },
      margin: 0,
      moltiplicator: 0.0001,
      loading: false,
      scrollPos: "",
    };
  }

  componentDidMount() {}

  handleTabChange(event: React.ChangeEvent<{}>, newValue: number): void {
    this.setState(
      {
        tabValue: newValue,
        orderType: newValue === 0 ? "sell" : "buy",
      },
      () => {
        if (this.state.firstValue && this.state.secondValue) {
          this.getCoupleInfo();
        }
      }
    );
  }

  getCoupleInfo() {
    const query = `&to_currency=${this.state.secondValue}&from_currency=${this.state.firstValue}`;
    API.forexGET("exange_rate", query).then((res) => {
      const responseInner = "Realtime Currency Exchange Rate";
      const ASK_PRICE = "9. Ask Price";
      const BID_PRICE = "8. Bid Price";

      if (res && res[responseInner]) {
        if (res[responseInner][BID_PRICE] && res[responseInner][ASK_PRICE]) {
          if (this.state.orderType === "sell") {
            this.setEnterPrice(res[responseInner][ASK_PRICE]);
            this.setStopLoss(res[responseInner][ASK_PRICE]);
            this.setTakeProfit(res[responseInner][ASK_PRICE]);
          } else {
            this.setEnterPrice(res[responseInner][BID_PRICE]);
            this.setStopLoss(res[responseInner][BID_PRICE]);
            this.setTakeProfit(res[responseInner][BID_PRICE]);
          }
        } else {
          console.log(
            "*** Error while retrieving forexGET(exange_rate) ask price or bid price. ***"
          );
        }
      } else {
        console.log("*** Error on forexGET(exange_rate) response. ***");
      }
    });
  }

  setEnterPrice(value: string | any, mustReturn: boolean = false) {
    let formatted = parseFloat(parseFloat(value).toFixed(5));
    if (mustReturn) {
      return formatted;
    }
    this.setState(
      {
        enterPrice: formatted,
      },
      () => {
        console.log("Enter updated:", this.state.enterPrice);
      }
    );
  }

  formatNumber(number: any): number {
    let parsed = parseFloat(number).toFixed(5);
    let final = parseFloat(parsed)

    return final
  }

  setStopLoss(value: string | any, mustReturn?: boolean) {
    let formatted = parseFloat(parseFloat(value).toFixed(5));

    if (this.state.orderType === "sell") {
      formatted += this.state.moltiplicator;
    } else {
      formatted -= this.state.moltiplicator;
    }

    if (mustReturn) {
      return formatted;
    }
    this.setState(
      {
        stopLoss: formatted.toFixed(5),
      },
      () => {
        console.log("Stop Loss updated: ", this.state.stopLoss);
      }
    );
  }

  setTakeProfit(value: string | any, mustReturn?: boolean) {
    let formatted = this.formatNumber(value)

    if (this.state.orderType === 'sell') {
      formatted -= this.state.moltiplicator;
    } else {
      formatted += this.state.moltiplicator;
    }

    if (mustReturn) {
      return formatted;
    }

    this.setState(
      {
        takeProfit: formatted.toFixed(5),
      },
      () => {
        console.log("Take Profit updated: ", this.state.takeProfit);
      }
    );
  }

  get returnEnterPrice(): Number | null {
    return this.state.enterPrice;
  }

  get scrollPosition() {
    console.log("scrollPos", this.state.scrollPos);
    return this.state.scrollPos;
  }

  resetStats(step2: boolean = false): void {
    if (step2) {
      this.setState(
        {
          rrr: {
            pipsInWin: 0,
            pipsInLose: 0,
            euroInWin: 0,
            euroInLose: 0,
          },
          margin: 0,
          proceedToStep2: step2,
          loading: false,
        },
        () => console.log("*** Stats reset ***")
      );
    } else {
      this.props.sendValues(null, null);
      this.setState(
        {
          rrr: {
            pipsInWin: 0,
            pipsInLose: 0,
            euroInWin: 0,
            euroInLose: 0,
          },
          margin: 0,
          enterPrice: null,
          stopLoss: null,
          takeProfit: null,
          proceedToStep2: step2,
          loading: false,
        },
        () => console.log("*** Stats reset ***")
      );
    }
  }

  async startCalc(): Promise<any> {
    if (
      (this.state.volumeValue && this.state.enterPrice,
      this.state.takeProfit,
      this.state.stopLoss)
    ) {
      await this.resetStats(true);
      
      let pipsDifferenceWin = 1;
      let pipsDifferenceLose = 1;
      let entrance: number | undefined = this.setEnterPrice(
        this.state.enterPrice,
        true
      );
      let take_profit: number | undefined = this.setTakeProfit(
        this.state.takeProfit,
        true
      );
      let stop_loss: number | undefined = this.setStopLoss(
        this.state.stopLoss,
        true
      );

      console.log("TP:", take_profit);
      console.log("SL:", stop_loss);

      // let moltiplicator = this.state.secondValue === "JPY" ? 0.01 : 0.0001;

      if (entrance && take_profit && stop_loss && this.state.volumeValue) {
        if (this.state.orderType && this.state.orderType === "sell") {
          pipsDifferenceWin =
            (entrance - take_profit) / this.state.moltiplicator;
          pipsDifferenceLose =
            (stop_loss - entrance) / this.state.moltiplicator;
          console.log(
            "Calculating for a SELL ORDER",
            pipsDifferenceWin,
            pipsDifferenceLose
          );
        } else {
          pipsDifferenceWin =
            (take_profit - entrance) / this.state.moltiplicator;
          pipsDifferenceLose =
            (entrance - stop_loss) / this.state.moltiplicator;
          console.log(
            "Calculating for a BUY ORDER",
            pipsDifferenceWin,
            pipsDifferenceLose
          );
        }

        let rrr = this.state.rrr;
        let valorePip = await this.calcSinglePip();
        if (valorePip) {
          rrr = {
            pipsInWin: pipsDifferenceWin,
            pipsInLose: pipsDifferenceLose,
            euroInLose: pipsDifferenceLose * valorePip,
            euroInWin: pipsDifferenceWin * valorePip,
          };
          this.setState({ rrr }, () => console.log("nuovi valori aggiornati"));
        } else {
          console.log(
            "*** Errore mentre cerco di recuperare il valore della singola PIP ***"
          );
        }

        this.calcMargin();
      }
    } else {
      console.log(
        "*** Error in: state.volumeValue || stopLoss || takepProfit || enterPrice. Controlla qui. ***"
      );
    }
  }

  async calcMargin() {
    let taxExange;
    let query = `&symbols=${this.state.firstValue}`;
    let margin: number = 1;
    let volume = 0;

    await API.forexDataFixerGet("latest", query).then((res: any) => {
      if (res && res.rates && res.rates[this.state.firstValue]) {
        taxExange = res.rates[this.state.firstValue];
        if (this.state.volumeValue) {
          volume = this.state.volumeValue;
        }
        margin = (volume / this.leva) * taxExange;

        this.setState({ margin: margin }, () =>
          console.log("*** Margine aggiornato ***", this.state.margin)
        );
      } else {
        console.log("*** Tasso di cambio non trovato ***");
      }
    });
  }

  async calcSinglePip() {
    let taxExange;
    let query = `&symbols=${this.state.secondValue}`;
    let valorePip: number = 1;
    let a: any = await API.forexDataFixerGet("latest", query).then(
      (res: any) => {
        if (res && res.rates && res.rates[this.state.secondValue]) {
          taxExange = res.rates[this.state.secondValue];
          if (this.state.volumeValue && taxExange) {
            // let moltiplicator =
            //   this.state.secondValue === "JPY" ? 0.01 : 0.0001;
            valorePip =
              this.state.volumeValue *
              (1 / taxExange) *
              this.state.moltiplicator;
            if (valorePip) {
              return valorePip;
            } else {
              console.log("Impossibile ritornare valorePip, errore.");
              return null;
            }
          }
        } else {
          console.log("TASSO DI CAMBIO NON TROVATO");
        }
      }
    );
    valorePip = a;
    return valorePip;
  }

  handleEnter(event: any, clicked: boolean = false, elementId: string) {
    if (!clicked) {
      if (event.keyCode === 13) {
        let el = document.getElementById(elementId);
        if (el) {
          el.focus();
        }
        // event.preventDefault();
      }
    } else {
      console.log("clicked");
      let el = document.getElementById(elementId);
      if (el) {
        el.focus();
      }
      // event.preventDefault();
    }
  }

  setMoltiplicator() {
    this.setState(
      {
        moltiplicator: this.state.secondValue === "JPY" ? 0.01 : 0.0001,
      },
      () => console.log("Moltiplicator updated:", this.state.moltiplicator)
    );
  }

  render() {
    return (
      <div>
        <Paper square>
          <Tabs
            variant="fullWidth"
            value={this.state.tabValue}
            indicatorColor="primary"
            textColor="primary"
            onChange={(event: any, newValue: number) => {
              this.handleTabChange(event, newValue);
            }}
          >
            <Tab label="Sell" />
            <Tab label="Buy" />
          </Tabs>
        </Paper>
        {this.state.loading ? (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              minHeight: "300px",
            }}
          >
            <CircularProgress />
          </div>
        ) : (
          <Grid container spacing={3}>
            <Grid item xs={6} lg={6}>
              <Autocomplete
                onKeyDown={(event) =>
                  this.handleEnter(event, false, "secondInput")
                }
                openOnFocus={true}
                onChange={(event, value) => {
                  this.setState(
                    {
                      firstValue: value,
                    },
                    () => this.handleEnter(event, true, "secondInput")
                  );
                }}
                options={Object.keys(this.props.items).map((option) => option)}
                renderInput={(params: any) => (
                  <TextField
                    {...params}
                    label="Prima valuta"
                    margin="normal"
                    variant="outlined"
                  />
                )}
              />
            </Grid>
            <Grid item xs={6} lg={6}>
              <Autocomplete
                id="secondInput"
                blurOnSelect={true}
                openOnFocus={true}
                onChange={(event, value: any) => {
                  this.setState(
                    {
                      secondValue: value,
                    },
                    () => {
                      this.props.sendValues(
                        this.state.firstValue,
                        this.state.secondValue
                      );
                      this.setMoltiplicator()
                      this.getCoupleInfo();
                    }
                  );
                }}
                // freeSolo
                options={Object.keys(this.props.items).map((option) => option)}
                renderInput={(params: any) => (
                  <TextField
                    {...params}
                    label="Seconda valuta"
                    margin="normal"
                    variant="outlined"
                  />
                )}
              />
            </Grid>
            {this.returnEnterPrice ? (
              <Grid item xs={12}>
                <Grid container spacing={3}>
                  <Grid item xs={12} lg={6}>
                    <form>
                      <TextField
                        fullWidth={true}
                        label="Dimesione scambio"
                        variant="outlined"
                        inputProps={{ step: "1000" }}
                        type="number"
                        value={this.state.volumeValue}
                        onChange={(event: any) => {
                          this.setState({
                            volumeValue: event.target.value,
                          });
                        }}
                      />
                    </form>
                  </Grid>
                  <Grid item xs={12} lg={6}>
                    <form>
                      <TextField
                        fullWidth={true}
                        label="Enter Price"
                        variant="outlined"
                        type="number"
                        inputProps={{ step: "0.001" }}
                        onChange={(event: any) => {
                          this.setState(
                            {
                              enterPrice: event.target.value,
                            },
                            () => {
                              this.setState({ prezzoOttenuto: true });
                            }
                          );
                        }}
                        // defaultValue={1}
                        value={this.returnEnterPrice}
                      />
                    </form>
                  </Grid>
                  <Grid item xs={12} lg={6}>
                    {this.returnEnterPrice ? (
                      <form>
                        <TextField
                          fullWidth={true}
                          label="Stop Loss"
                          variant="outlined"
                          inputProps={{ step: "0.001" }}
                          defaultValue={this.setStopLoss(
                            this.state.enterPrice,
                            true
                          )}
                          type="number"
                          onChange={(event: any) => {
                            this.setState({
                              stopLoss: event.target.value,
                            });
                          }}
                        />
                      </form>
                    ) : (
                      <CircularProgress />
                    )}
                  </Grid>
                  <Grid item xs={12} lg={6}>
                    {this.returnEnterPrice ? (
                      <form>
                        <TextField
                          fullWidth={true}
                          label="Take Profit"
                          variant="outlined"
                          inputProps={{ step: "0.001" }}
                          defaultValue={this.setTakeProfit(
                            this.state.enterPrice,
                            true
                          )}
                          type="number"
                          onChange={(event: any) => {
                            this.setState({
                              takeProfit: event.target.value,
                            });
                          }}
                        />
                      </form>
                    ) : (
                      <CircularProgress />
                    )}
                  </Grid>
                  <Grid style={{ marginTop: 32, marginLeft: "auto" }}>
                    <Button
                      style={{ marginLeft: 10 }}
                      onClick={() => {
                        this.setState(
                          {
                            loading: true,
                          },
                          () => {
                            setTimeout(() => {
                              this.resetStats(false);
                            }, 1000);
                          }
                        );
                      }}
                      variant="outlined"
                      color="secondary"
                    >
                      Reset
                    </Button>
                    <Button
                      style={{ marginLeft: 10 }}
                      onClick={() => {
                        if (
                          (this.state.volumeValue && this.state.enterPrice,
                          this.state.takeProfit,
                          this.state.stopLoss)
                        ) {
                          this.setState({
                            proceedToStep2: true,
                          });
                          this.startCalc();
                        } else {
                          console.log("Impossibile procedere al calcolo");
                        }
                      }}
                      variant="contained"
                      color="primary"
                    >
                      Calcola
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            ) : null}
          </Grid>
        )}
        {this.state.proceedToStep2 ? (
          <Grid
            container
            spacing={3}
            style={{ marginTop: 32, justifyContent: "center" }}
          >
            <Grid item xs={12} md={4}>
              <Card>
                <CardHeader
                  avatar={
                    <>
                      <Box display={{ xs: "none", sm: "block" }}>
                        <Typography variant="h6" component="h4">
                          {this.state.firstValue}/{this.state.secondValue}
                        </Typography>
                        <Typography color="textSecondary">
                          {this.state.orderType.toUpperCase() + " "} Order
                        </Typography>
                      </Box>
                    </>
                  }
                  subheader={
                    <>
                      <List component="nav" aria-label="main mailbox folders">
                        <ListItem button>
                          <ListItemIcon>
                            <Tooltip
                              title="Enter Price"
                              aria-label="Enter Price"
                            >
                              <LocalAtmIcon />
                            </Tooltip>
                          </ListItemIcon>
                          <ListItemText primary={this.state.enterPrice} />
                        </ListItem>
                        <ListItem button>
                          <ListItemIcon>
                            <Tooltip
                              title="Dimensione scambio"
                              aria-label="Dimensione scambio"
                            >
                              <EqualizerIcon />
                            </Tooltip>
                          </ListItemIcon>
                          <ListItemText primary={this.state.volumeValue} />
                        </ListItem>
                        <ListItem button>
                          <ListItemIcon>
                            <Tooltip title="Stop Loss" aria-label="Stop Loss">
                              <PanToolIcon />
                            </Tooltip>
                          </ListItemIcon>
                          <ListItemText primary={this.state.stopLoss} />
                        </ListItem>
                        <ListItem button>
                          <ListItemIcon>
                            <Tooltip
                              title="Take Profit"
                              aria-label="Take Profit"
                            >
                              <AssignmentTurnedInIcon />
                            </Tooltip>
                          </ListItemIcon>
                          <ListItemText primary={this.state.takeProfit} />
                        </ListItem>
                      </List>
                    </>
                  }
                />
              </Card>
            </Grid>
            <Grid
              item
              xs={12}
              md={4}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {this.state.rrr &&
              this.state.rrr.pipsInWin &&
              this.state.rrr.pipsInLose ? (
                <div style={{ flex: 1 }}>
                  <Card>
                    <CardHeader
                      subheader={
                        <>
                          <Typography color="textSecondary">PIPs</Typography>
                          <List
                            component="nav"
                            aria-label="main mailbox folders"
                          >
                            <ListItem button>
                              <ListItemIcon>
                                <ControlPointIcon />
                              </ListItemIcon>
                              <ListItemText
                                primary={Math.round(this.state.rrr.pipsInWin)}
                                secondary="profitto"
                              />
                            </ListItem>
                            <ListItem button>
                              <ListItemIcon>
                                <RemoveIcon />
                              </ListItemIcon>
                              <ListItemText
                                primary={Math.round(this.state.rrr.pipsInLose)}
                                secondary="perdita"
                              />
                            </ListItem>
                            <Divider />
                          </List>
                          <Typography color="textSecondary">Euro</Typography>
                          <List
                            component="nav"
                            aria-label="main mailbox folders"
                          >
                            <ListItem button>
                              <ListItemIcon>
                                <ControlPointIcon />
                              </ListItemIcon>
                              <ListItemText
                                primary={this.state.rrr.euroInWin.toFixed(2)}
                                secondary="profitto"
                              />
                            </ListItem>
                            <ListItem button>
                              <ListItemIcon>
                                <RemoveIcon />
                              </ListItemIcon>
                              <ListItemText
                                primary={this.state.rrr.euroInLose.toFixed(2)}
                                secondary="perdita"
                              />
                            </ListItem>
                          </List>
                        </>
                      }
                    />
                  </Card>
                </div>
              ) : (
                <CircularProgress />
              )}
            </Grid>
            <Grid item xs={12} sm={"auto"}>
              {this.state.margin ? (
                <div>
                  <Card>
                    <CardHeader
                      avatar={
                        <>
                          <AccountBalanceWalletIcon />
                          <Typography color="textSecondary" gutterBottom>
                            Margine
                          </Typography>
                        </>
                      }
                      subheader={
                        <Typography variant="h5" component="h2">
                          {this.state.margin.toFixed(2)} euro
                        </Typography>
                      }
                    />
                  </Card>
                </div>
              ) : null}
            </Grid>
          </Grid>
        ) : null}
      </div>
    );
  }
}
