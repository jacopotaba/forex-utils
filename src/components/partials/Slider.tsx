import React from "react";
import Swiper, { SwiperOptions } from "swiper";
import "swiper/swiper.scss";
import API from "../../api/ApiService";

interface SliderPropsInterface {
  query: string;
  items?: any[];
}
class Slider extends React.Component<SliderPropsInterface, { items: any[] }> {
  constructor(props: SliderPropsInterface) {
    super(props);
    this.state = {
      items: [],
    };
  }

  baseUrlImage = "https://image.tmdb.org/t/p/original";

  imgStyle = {
    width: "100%",
    height: "auto",
  };

  swiper: any;

  swiperSettings: SwiperOptions = {
    slidesPerView: 4,
    spaceBetween: 16,
  };

  componentDidMount() {
  }

  loadMovies(query?: string) {
    setTimeout(() => {
      // API.makeGetRequest(query).then((res: any) => {
      //   this.setState({ items: res.results });
      //   if (res && res.results && res.results.length) {
      //     this.swiper = new Swiper(".swiper-container", this.swiperSettings);
      //   }
      // });
    });
  }

  render() {
    return (
      <>
        <div className="swiper-container">
          <div className="swiper-wrapper">
            {this.state.items && this.state.items.length
              ? this.state.items.map((res: any, index: number) => {
                  return (
                    <div className="swiper-slide" key={index}>
                      <img
                        style={this.imgStyle}
                        src={this.baseUrlImage + res.backdrop_path}
                        alt=""
                      />
                    </div>
                  );
                })
              : null}
          </div>
        </div>
      </>
    );
  }
}

export default Slider;
