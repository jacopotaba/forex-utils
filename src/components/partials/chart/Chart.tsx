import React from "react";
import { Grid } from "@material-ui/core";
// @ts-ignore
import TradingViewWidget from "react-tradingview-widget";

export default class Chart extends React.Component<
  { value1: any; value2: any },
  any
> {
  chart: any;
  chartRef: any;
  lineSeries: any;
  candleSeries: any;

  responseInner: any = "Time Series FX (60min)";

  constructor(props: any) {
    super(props);

    this.state = {
      candles: [],
    };
  }

  componentDidMount() {}

  render() {
    return (
      <>
        <Grid style={{ marginTop: 64, marginBottom: 64 }} container spacing={3}>
          <Grid
            item
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              width: "100%",
              height: 600,
            }}
          >
            {this.props.value1 && this.props.value2 ? (
              <TradingViewWidget
                popup_width="100%"
                autosize
                symbol={`${this.props.value1}${this.props.value2}`}
                locale="it"
              />
            ) : null}
          </Grid>
        </Grid>
      </>
    );
  }
}
