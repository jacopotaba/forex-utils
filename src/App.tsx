import React from 'react';
import './App.scss';
import AppContainer from './containers/AppContainer/AppContainer'

function App() {
  return (
    <>
    <AppContainer />
    </>
  );
}

export default App;
