/* data.fixer rest api */
const dataFixerBaseURL = `http://data.fixer.io/api/`;
const apiKey = "?access_key=3a6c260452b71f595d056141ebecea0a&format=1";
// const headers = {
//   "Access-Control-Allow-Origin": "*",
//   "Content-Type": "application/json",
// };

/*
 * Alpha Vantage Rest Api: https://www.alphavantage.co/documentation/#
 */

const alphaVantageURL = "https://alpha-vantage.p.rapidapi.com/";
const alphaVantageHeaders = {
  method: "GET",
  headers: {
    "x-rapidapi-key": "26c2c9c5d3mshcd697dccaac11fap1cfef9jsn6af7fa867f13",
    "x-rapidapi-host": "alpha-vantage.p.rapidapi.com",
  },
};

const FOREX_MAP: any = {
  exange_rate: {
    functionName: "CURRENCY_EXCHANGE_RATE",
    /*** example query: '&to_currency=JPY&from_currency=USD' ***/
    params: {
      from_currency: "",
      to_currency: "",
    },
  },
  fx_intraday: {
    functionName: "FX_INTRADAY",
    /** example query: '&to_symbol=USD&interval=5min&from_symbol=EUR&datatype=json&outputsize=compact' **/
  },
};

async function forexDataFixerGet(queryParams: any, filters?: any) {
  // const fullUrl = `http://data.fixer.io/api/latest?access_key=3a6c260452b71f595d056141ebecea0a&format=1`;
  const fullUrl = `${dataFixerBaseURL}${queryParams}${apiKey}${filters}`;
  const response: any = await fetch(fullUrl, {
    method: "GET",
  });

  const items = await response.json();
  if (response.ok) {
    return items;
  } else {
    let err = { status: response.status, errObj: items };
    throw err;
  }
}

async function forexGET(functionType: string, params: any) {
  let functionName = FOREX_MAP[functionType].functionName;
  let query = `query?function=${functionName}&${params}`;
  const fullUrl = `${alphaVantageURL}${query}`;
  const response: any = await fetch(fullUrl, { ...alphaVantageHeaders });
  const items = await response.json();
  if (response.ok) {
    return items;
  } else {
    let err = { status: response.status, errObj: items };
    throw err;
  }
}

// const fullUrl = `${dataFixerBaseURL} ${apiKey}`;

const API = {
  forexDataFixerGet,
  forexGET,
};

export default API;

// async function makePostRequest(method: string, query: string, postData: any) {
//   const fullUrl = `${dataFixerBaseURL}${query}${apiKey}`;
//   const response: any = await fetch(fullUrl, {
//     method: "POST",
//     headers,
//     body: JSON.stringify(postData),
//   });

//   const items = await response.json();
//   if (response.ok) {
//     return items;
//   } else {
//     let err = { status: response.status, errObj: items };
//     throw err;
//   }
// }

// async function makeGetRequest(queryParams: any) {
//   // const fullUrl = `http://data.fixer.io/api/latest?access_key=3a6c260452b71f595d056141ebecea0a&format=1`;
//   const fullUrl = `${dataFixerBaseURL}${queryParams}${apiKey}`;
//   const response: any = await fetch(fullUrl, {
//     method: "GET",
//     headers,
//   });

//   const items = await response.json();
//   if (response.ok) {
//     return items;
//   } else {
//     let err = { status: response.status, errObj: items };
//     throw err;
//   }
// }
