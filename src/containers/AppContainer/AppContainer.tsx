import React from "react";
import Container from "../../components/layout/containers/Container";
import Header from "../../components/layout/Header";
import MainNavigation from "../../components/layout/MainNavigation";
import ScrollToTop from "../../components/layout/ScrollToTop";

class AppContainer extends React.Component<any, {windowScroll: number}> {
  constructor(props: any) {
    super(props);
    this.state = {
      windowScroll: 0
    };
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll.bind(this));
  }

  handleScroll(event: any) {
    this.setState({
      windowScroll: window.pageYOffset,
    });
  }

  render() {
    return (
      <>
        {/* <ResponsiveDrawer></ResponsiveDrawer> */}
        <Header />
        <Container>
          <MainNavigation />
        </Container>
        {
          this.state.windowScroll && this.state.windowScroll > 20 ? ( <ScrollToTop></ScrollToTop>) : null
        }
      </>
    );
  }
}

export default AppContainer;
