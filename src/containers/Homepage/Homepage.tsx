import React from "react";
import API from "../../api/ApiService";
import Chart from "../../components/partials/chart/Chart";
import NuovoOrdine from "../../components/partials/forex/NuovoOrdine";

class Homepage extends React.Component<
  {},
  { values: {}; value1: any; value2: any }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      values: {},
      value1: "",
      value2: "",
    };
  }

  componentDidMount() {
    API.forexDataFixerGet("symbols", "USD").then((res) => {
      this.setState(
        {
          values: res.symbols,
        },
        () => {
          console.log("Home: symbols retrieved");
        }
      );

      if (!localStorage.getItem("preferredItems")) {
        localStorage.setItem(
          "preferredItems",
          JSON.stringify({
            EUR: "Eur",
            USD: "United States Dollar",
            GBP: "British Pound Sterling",
            CAD: "Canadian Dollar",
            AUD: "Australian Dollar",
            CHF: "Swiss Franc",
            JPY: "Japanese Yen",
            NZD: "New Zealand Dollar",
          })
        );
      } else {
        let values = this.state.values;
        const preferred = localStorage.getItem("preferredItems") || "";
        const parsed = JSON.parse(preferred);
        Object.assign(parsed, values);
        values = parsed;
        this.setState({ values });
      }
    });
  }

  getValues(value1: any, value2: any) {
    if (value1 && value2) {
      this.setState({
        value1: value1,
        value2: value2,
      });
    } else {
      this.setState({
        value1: null,
        value2: null,
      });
    }
  }

  render() {
    return (
      <>
        <NuovoOrdine
          sendValues={this.getValues.bind(this)}
          items={this.state.values}
        ></NuovoOrdine>
        {this.state.value1 && this.state.value2 ? (
          <Chart value1={this.state.value1} value2={this.state.value2}></Chart>
        ) : null}
        {/* <CalcolaPip items={this.state.values}></CalcolaPip> */}
      </>
    );
  }
}

export default Homepage;
